<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('status')->default(0);
            $table->string('project_name', 80)->nullable(); // Название проекта
            $table->string('contact')->nullable(); // Контакт
            $table->string('company')->nullable();// Компания
            $table->string('entity')->nullable(); // юрлицо
            $table->string('photographer')->nullable(); // юрлицо
            $table->string('assistants')->nullable(); // юрлицо
            $table->text('comment')->nullable();
            $table->text('short_comment')->nullable(); //короткий коммент
            $table->integer('number_of_assistant')->default(0);
            $table->boolean('have_assistant')->nullable();
            $table->boolean('have_equipment')->nullable();
            $table->boolean('have_delivery')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->string('address')->nullable();
            

            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();

            /*            

            Кнопка «Создать».

            */ 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
