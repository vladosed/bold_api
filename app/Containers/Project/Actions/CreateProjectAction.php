<?php

namespace App\Containers\Project\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateProjectAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->all();
        $dates = $request->dates;

        if(empty($dates)){ // only project
            $project = Apiato::call('Project@CreateProjectTask', [$data]);
            return $project;
        }
        else{ // project with dates
            $project = Apiato::call('Project@CreateProjectTask', [$data]);

            foreach ($dates as &$project_date) {
                $project_date['project_id'] = $project->id; 
                Apiato::call('Date@CreateDateTask', [$project_date]);
            }
            return $project;
        }
    }
}