<?php

namespace App\Containers\Project\Models;

use Apiato\Core\Traits\HashIdTrait;
use Apiato\Core\Traits\HasResourceKeyTrait;
use App\Ship\Parents\Models\Model;
use App\Containers\Date\Models\Date;

class Project extends Model
{


    protected $fillable = [
        'id',
        'project_name',
        'contact',
        'company',
        'entity',
        'photographer',
        'assistants',
        'number_of_assistant',
        'have_assistant',
        'have_equipment',
        'have_delivery',
        'short_comment',
        'confirmed',
        'address',
    ];

  /*  protected $attributes = [
    ];

    protected $hidden = [

    ];

    protected $casts = [
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];*/

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'projects';
    
    public function dates()
    {
        return $this->hasMany(Date::class);
    }
}
