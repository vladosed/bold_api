<?php

namespace App\Containers\Project\UI\API\Transformers;

use App\Containers\Date\UI\API\Transformers\DateTransformer;
use App\Containers\Project\Models\Project;
use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Project\UI\API\Transformers\ProjectTransformer;
use App\Containers\Date\Models\Date;


class ProjectTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [
        'date',
    ];

    /**
     * @param Project $entity
     *
     * @return array
     */
    public function transform(Project $entity)
    {
        $response = [
            'object'         => 'Project',
            'id'             => $entity->id,
            'project_name'   => $entity->project_name,
            'contact'        => $entity->contact,
            'company'        => $entity->company,
            'entity'         => $entity->entity,
            'photographer'   => $entity->photographer,
            'assistants'     => $entity->assistants,
            'short_comment'  => $entity->short_comment,
            'number_of_assistant' => $entity->number_of_assistant,
            'have_assistant' => $entity->have_assistant,
            'have_equipment' => $entity->have_equipment,
            'have_delivery'  => $entity->have_delivery,
            'created_at'     => $entity->created_at,
            'updated_at'     => $entity->updated_at,
            'confirmed'     => $entity->confirmed,
            'address'     => $entity->address,

        ];
        

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    public function includeDate(Project $project)
    {
        return $this->collection($project->dates, new DateTransformer());
    }
}
