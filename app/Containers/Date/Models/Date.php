<?php

namespace App\Containers\Date\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Parents\Models\Parents;
use App\Containers\Project\Models\Project;

class Date extends Model
{
    protected $fillable = [
        'project_id',
        'start',
        'end'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'dates';

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
