<?php

namespace App\Containers\Date\UI\API\Transformers;

use App\Containers\Date\Models\Date;
use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Project\UI\API\Transformers\ProjectTransformer;
use App\Containers\Project\Models\Project;

class DateTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [
        'project'
    ];

    /**
     * @param Date $entity
     *
     * @return array
     */
    public function transform(Date $entity)
    {
        $response = [
            'object' => 'Date',
            'id' => $entity->id,
            'start' => $entity->start,
            'end' => $entity->end,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    public function includeProject(Date $date)
    {
        return $this->item($date->project, new ProjectTransformer());
    }
}
