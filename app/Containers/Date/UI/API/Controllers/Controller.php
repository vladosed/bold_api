<?php

namespace App\Containers\Date\UI\API\Controllers;

use App\Containers\Date\UI\API\Requests\CreateDateRequest;
use App\Containers\Date\UI\API\Requests\DeleteDateRequest;
use App\Containers\Date\UI\API\Requests\GetAllDatesRequest;
use App\Containers\Date\UI\API\Requests\FindDateByIdRequest;
use App\Containers\Date\UI\API\Requests\UpdateDateRequest;
use App\Containers\Date\UI\API\Transformers\DateTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Date\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateDateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDate(CreateDateRequest $request)
    {
        $date = Apiato::call('Date@CreateDateAction', [$request]);

        return $this->created($this->transform($date, DateTransformer::class));
    }

    /**
     * @param FindDateByIdRequest $request
     * @return array
     */
    public function findDateById(FindDateByIdRequest $request)
    {
        $date = Apiato::call('Date@FindDateByIdAction', [$request]);

        return $this->transform($date, DateTransformer::class);
    }

    /**
     * @param GetAllDatesRequest $request
     * @return array
     */
    public function getAllDates(GetAllDatesRequest $request)
    {
        $dates = Apiato::call('Date@GetAllDatesAction', [$request]);

        return $this->transform($dates, DateTransformer::class);
    }

    /**
     * @param UpdateDateRequest $request
     * @return array
     */
    public function updateDate(UpdateDateRequest $request)
    {
        $date = Apiato::call('Date@UpdateDateAction', [$request]);

        return $this->transform($date, DateTransformer::class);
    }

    /**
     * @param DeleteDateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDate(DeleteDateRequest $request)
    {
        Apiato::call('Date@DeleteDateAction', [$request]);

        return $this->noContent();
    }
}
