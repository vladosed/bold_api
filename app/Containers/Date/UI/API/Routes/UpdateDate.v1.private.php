<?php

/**
 * @apiGroup           Date
 * @apiName            updateDate
 *
 * @api                {PATCH} /v1/dates/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
/*$router->patch('dates/{id}', [
    'as' => 'api_date_update_date',
    'uses'  => 'Controller@updateDate',
    'middleware' => [
      'auth:api',
    ],
]);*/

$router->patch('dates/{id}', [
  'as' => 'api_date_update_date',
  'uses'  => 'Controller@updateDate',
]);
