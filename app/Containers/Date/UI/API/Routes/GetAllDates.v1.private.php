<?php

/**
 * @apiGroup           Date
 * @apiName            getAllDates
 *
 * @api                {GET} /v1/dates Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
/*$router->get('dates', [
    'as' => 'api_date_get_all_dates',
    'uses'  => 'Controller@getAllDates',
    'middleware' => [
      'auth:api',
    ],
]);*/
$router->get('dates', [
  'as' => 'api_date_get_all_dates',
  'uses'  => 'Controller@getAllDates',
]);
