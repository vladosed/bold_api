<?php

/**
 * @apiGroup           Date
 * @apiName            findDateById
 *
 * @api                {GET} /v1/dates/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
/*$router->get('dates/{id}', [
    'as' => 'api_date_find_date_by_id',
    'uses'  => 'Controller@findDateById',
    'middleware' => [
      'auth:api',
    ],
]);*/

$router->get('dates/{id}', [
  'as' => 'api_date_find_date_by_id',
  'uses'  => 'Controller@findDateById',
]);
