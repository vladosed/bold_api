<?php

/**
 * @apiGroup           Date
 * @apiName            deleteDate
 *
 * @api                {DELETE} /v1/dates/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
/*$router->delete('dates/{id}', [
    'as' => 'api_date_delete_date',
    'uses'  => 'Controller@deleteDate',
    'middleware' => [
      'auth:api',
    ],
]);*/

$router->delete('dates/{id}', [
  'as' => 'api_date_delete_date',
  'uses'  => 'Controller@deleteDate',
]);
