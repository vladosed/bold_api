<?php

/**
 * @apiGroup           Date
 * @apiName            createDate
 *
 * @api                {POST} /v1/dates Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
/*$router->post('dates', [
    'as' => 'api_date_create_date',
    'uses'  => 'Controller@createDate',
    'middleware' => [
      'auth:api',
    ],
]);*/
$router->post('dates', [
  'as' => 'api_date_create_date',
  'uses'  => 'Controller@createDate',
]);
