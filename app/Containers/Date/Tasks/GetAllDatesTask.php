<?php

namespace App\Containers\Date\Tasks;

use App\Containers\Date\Data\Repositories\DateRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllDatesTask extends Task
{

    protected $repository;

    public function __construct(DateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate(500);
    }
}