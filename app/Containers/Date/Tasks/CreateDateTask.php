<?php

namespace App\Containers\Date\Tasks;

use App\Containers\Date\Data\Repositories\DateRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateDateTask extends Task
{

    protected $repository;

    public function __construct(DateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
