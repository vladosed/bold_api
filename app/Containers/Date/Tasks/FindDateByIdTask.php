<?php

namespace App\Containers\Date\Tasks;

use App\Containers\Date\Data\Repositories\DateRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindDateByIdTask extends Task
{

    protected $repository;

    public function __construct(DateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
