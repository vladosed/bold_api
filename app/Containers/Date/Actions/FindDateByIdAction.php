<?php

namespace App\Containers\Date\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindDateByIdAction extends Action
{
    public function run(Request $request)
    {
        $date = Apiato::call('Date@FindDateByIdTask', [$request->id]);

        return $date;
    }
}
