<?php

namespace App\Containers\Date\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteDateAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Date@DeleteDateTask', [$request->id]);
    }
}
