<?php

namespace App\Containers\Date\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateDateAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->all();

        $date = Apiato::call('Date@CreateDateTask', [$data]);

        return $date;
    }
}
