<?php

namespace App\Containers\Date\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateDateAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->all();

        $date = Apiato::call('Date@UpdateDateTask', [$request->id, $data]);

        return $date;
    }
}
