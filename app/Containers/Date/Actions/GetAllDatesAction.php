<?php

namespace App\Containers\Date\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllDatesAction extends Action
{
    public function run(Request $request)
    {
        $dates = Apiato::call('Date@GetAllDatesTask', [], ['addRequestCriteria']);

        return $dates;
    }
}
