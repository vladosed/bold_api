<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDateTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('dates', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('project_id');
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();

            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('dates');
    }
}
