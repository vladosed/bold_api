<?php

namespace App\Containers\Date\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DateRepository
 */
class DateRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
